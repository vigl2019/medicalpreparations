package com.example.medicalpreparations.utils;

import com.example.medicalpreparations.model.MedicalPreparation;

import java.util.List;

public interface IMedicalPreparations {
    void getMedicalPreparations(List<MedicalPreparation> medicalPreparations);
}
