package com.example.medicalpreparations.utils;

import com.example.medicalpreparations.model.MedicalPreparation;

public interface IMedicalPreparation {
    void getMedicalPreparation(MedicalPreparation medicalPreparation);
}
