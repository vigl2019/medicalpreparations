package com.example.medicalpreparations.utils;

import android.app.Application;
import android.content.Context;

public class App extends Application {
    public static App instance;
    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        context = App.this;
    }
    public static App getInstance() {
        return instance;
    }
    public Context getContext() {
        return context;
    }
}

