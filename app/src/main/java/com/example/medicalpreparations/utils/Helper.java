package com.example.medicalpreparations.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Helper {

    public static String getMedicalPreparationTitle(String queryString){
        String[] query = queryString.split("\n");
        return query[0].trim();
    }

    public static void createAlertDialog(String errorMassage, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}