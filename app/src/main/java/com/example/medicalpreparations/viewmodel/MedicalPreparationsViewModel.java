package com.example.medicalpreparations.viewmodel;

import android.view.View;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.medicalpreparations.R;
import com.example.medicalpreparations.adapter.MedicalPreparationsAdapter;
import com.example.medicalpreparations.model.MedicalPreparation;
import com.example.medicalpreparations.model.MedicalPreparations;

import java.util.List;

public class MedicalPreparationsViewModel extends ViewModel {

    private MedicalPreparations medicalPreparations;
    private MedicalPreparationsAdapter medicalPreparationsAdapter;
    public MutableLiveData<MedicalPreparation> selected;
    public ObservableInt showEmpty;

    public void init() {
        medicalPreparations = new MedicalPreparations();
        medicalPreparationsAdapter = new MedicalPreparationsAdapter(R.layout.medical_preparation_view, this);
        selected = new MutableLiveData<>();
        showEmpty = new ObservableInt(View.GONE);
    }

    public void fetchMedicalPreparationList() {
        medicalPreparations.fetchMedicalPreparationList();
    }

/*
    public void fetchMedicalPreparationListByTitle(String medicalPreparationTitle){
        medicalPreparations.fetchMedicalPreparationListByTitle(medicalPreparationTitle);
    }
*/

    public MutableLiveData<List<MedicalPreparation>> getMedicalPreparationsLiveData() {
        return medicalPreparations.getMedicalPreparationsLiveData();
    }

    public MutableLiveData<List<MedicalPreparation>> getMedicalPreparationsLiveDataByTitle() {
        return medicalPreparations.getMedicalPreparationsLiveDataByTitle();
    }

    public MedicalPreparationsAdapter getMedicalPreparationsAdapter() {
        return medicalPreparationsAdapter;
    }

    public void reloadAdapter(){
        medicalPreparationsAdapter.notifyDataSetChanged();
    }

    public void setMedicalPreparationsInAdapter(List<MedicalPreparation> medicalPreparations) {
        this.medicalPreparationsAdapter.setMedicalPreparations(medicalPreparations);
        this.medicalPreparationsAdapter.notifyDataSetChanged();
    }

    public MutableLiveData<MedicalPreparation> getSelected() {
        return selected;
    }

    public void onItemClick(Integer index) {
        MedicalPreparation medicalPreparation = getMedicalPreparationAt(index);
        selected.setValue(medicalPreparation);
    }

    public MedicalPreparation getMedicalPreparationAt(Integer index) {

        if (medicalPreparations.getMedicalPreparationsLiveData().getValue() != null && index != null
                && medicalPreparations.getMedicalPreparationsLiveData().getValue().size() > index) {
            return medicalPreparations.getMedicalPreparationsLiveData().getValue().get(index);
        }
        return null;
    }
}