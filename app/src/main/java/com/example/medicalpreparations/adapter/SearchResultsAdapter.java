package com.example.medicalpreparations.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medicalpreparations.R;
import com.example.medicalpreparations.model.MedicalPreparation;

import java.util.List;

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.SearchResultsViewHolder> {

    TextView tradeLabel;
    TextView manufacturerName;
    TextView packagingDescription;
    TextView compositionDescription;
    TextView compositionInn;
    TextView compositionPharmForm;

    private List<MedicalPreparation> medicalPreparations;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onClick(MedicalPreparation medicalPreparation);
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public SearchResultsAdapter(List<MedicalPreparation> medicalPreparations) {
        this.medicalPreparations = medicalPreparations;
    }

    @NonNull
    @Override
    public SearchResultsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_results_items, parent, false);

        tradeLabel = cardView.findViewById(R.id.sri_trade_label);
        manufacturerName = cardView.findViewById(R.id.sri_manufacturer_name);
        packagingDescription = cardView.findViewById(R.id.sri_packaging_description);
        compositionDescription = cardView.findViewById(R.id.sri_composition_description);
        compositionInn = cardView.findViewById(R.id.sri_composition_inn);
        compositionPharmForm = cardView.findViewById(R.id.sri_composition_pharm_form);

        return new SearchResultsViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultsViewHolder holder, int position) {
        MedicalPreparation medicalPreparation = medicalPreparations.get(position);

        tradeLabel.setText(medicalPreparation.getTradeLabel());
        manufacturerName.setText(medicalPreparation.getManufacturerName());
        packagingDescription.setText(medicalPreparation.getPackagingDescription());
        compositionDescription.setText(medicalPreparation.getCompositionDescription());
        compositionInn.setText(medicalPreparation.getCompositionInn());
        compositionPharmForm.setText(medicalPreparation.getCompositionPharmForm());
    }

    @Override
    public int getItemCount() {
        return medicalPreparations.size();
    }

    class SearchResultsViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        public SearchResultsViewHolder(@NonNull CardView itemView) {
            super(itemView);

            cardView = itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClick(medicalPreparations.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
