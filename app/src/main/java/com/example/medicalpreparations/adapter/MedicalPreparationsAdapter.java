package com.example.medicalpreparations.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medicalpreparations.BR;
import com.example.medicalpreparations.model.MedicalPreparation;
import com.example.medicalpreparations.viewmodel.MedicalPreparationsViewModel;

import java.util.List;

public class MedicalPreparationsAdapter extends RecyclerView.Adapter<MedicalPreparationsAdapter.MedicalPreparationsViewHolder> {

    private int layoutId;
    private List<MedicalPreparation> medicalPreparations;
    private MedicalPreparationsViewModel medicalPreparationsViewModel;

    public MedicalPreparationsAdapter(int layoutId, MedicalPreparationsViewModel medicalPreparationsViewModel) {
        this.layoutId = layoutId;
        this.medicalPreparationsViewModel = medicalPreparationsViewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return medicalPreparations == null ? 0 : medicalPreparations.size();
    }

    @NonNull
    @Override
    public MedicalPreparationsAdapter.MedicalPreparationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return new MedicalPreparationsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicalPreparationsAdapter.MedicalPreparationsViewHolder holder, int position) {
        holder.bind(medicalPreparationsViewModel, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    public void setMedicalPreparations(List<MedicalPreparation> medicalPreparations){
        this.medicalPreparations = medicalPreparations;
    }

    class MedicalPreparationsViewHolder extends RecyclerView.ViewHolder
    {
        final ViewDataBinding binding;

        MedicalPreparationsViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind (MedicalPreparationsViewModel viewModel, Integer position){
            viewModel.getMedicalPreparationAt(position);
            binding.setVariable(BR.viewModel, viewModel);
            binding.setVariable(BR.position, position);
            binding.executePendingBindings();
        }
    }
}
