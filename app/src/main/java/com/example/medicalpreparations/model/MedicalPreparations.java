package com.example.medicalpreparations.model;

import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;

import com.example.medicalpreparations.R;
import com.example.medicalpreparations.api.ApiService;
import com.example.medicalpreparations.utils.App;
import com.example.medicalpreparations.utils.IMedicalPreparation;
import com.example.medicalpreparations.utils.IMedicalPreparations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class MedicalPreparations extends BaseObservable {

    private MutableLiveData<List<MedicalPreparation>> medicalPreparationsLiveData = new MutableLiveData<>();
    private MutableLiveData<List<MedicalPreparation>> medicalPreparationsLiveDataByTitle = new MutableLiveData<>();
    private MutableLiveData<MedicalPreparation> medicalPreparationLiveDataById = new MutableLiveData<>();
    private CompositeDisposable disposables = new CompositeDisposable();

    public MutableLiveData<List<MedicalPreparation>> getMedicalPreparationsLiveData() {
        return medicalPreparationsLiveData;
    }

    public MutableLiveData<List<MedicalPreparation>> getMedicalPreparationsLiveDataByTitle() {
        return medicalPreparationsLiveDataByTitle;
    }

    public MutableLiveData<MedicalPreparation> getMedicalPreparationLiveDataById() {
        return medicalPreparationLiveDataById;
    }

    public void fetchMedicalPreparationListByTitle(String medicalPreparationTitle, final IMedicalPreparations iMedicalPreparations) {
        disposables.add(ApiService.getMedicalPreparationByTitle(medicalPreparationTitle)

                .map(new Function<ResponseModel, List<MedicalPreparation>>() {
                    @Override
                    public List<MedicalPreparation> apply(ResponseModel responseModel) {
                        final List<MedicalPreparation> medicalPreparationList = convert(responseModel);
                        return medicalPreparationList;
                    }
                })

                .filter(new Predicate<List<MedicalPreparation>>() {
                    @Override
                    public boolean test(List<MedicalPreparation> medicalPreparationList) throws Exception {

                        boolean isMedicalPreparationListHaveValues = ((medicalPreparationList != null) && (!medicalPreparationList.isEmpty()));

                        if (!isMedicalPreparationListHaveValues) {
                            Log.i(App.getInstance().getString(R.string.TAG_get_Preparations_By_Title),
                                    App.getInstance().getString(R.string.list_med_preparation_not_values));
                        }

                        return isMedicalPreparationListHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<MedicalPreparation>>() {
                    @Override
                    public void accept(List<MedicalPreparation> medicalPreparationList) throws Exception {
                        medicalPreparationsLiveDataByTitle.setValue(medicalPreparationList);
                        iMedicalPreparations.getMedicalPreparations(medicalPreparationList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e(App.getInstance().getString(R.string.TAG_get_Preparations_By_Title),
                                throwable.getMessage(), throwable);
                    }
                }));
    }

    public void fetchMedicalPreparationById(String medicalPreparationId, final IMedicalPreparation iMedicalPreparation) {
        disposables.add(ApiService.getMedicalPreparationById(medicalPreparationId)

                .map(new Function<ResponseModelId, MedicalPreparation>() {
                    @Override
                    public MedicalPreparation apply(ResponseModelId responseModelId) {
                        final MedicalPreparation medicalPreparation = convertResponseModelId(responseModelId);
                        return medicalPreparation;
                    }
                })

                .filter(new Predicate<MedicalPreparation>() {
                    @Override
                    public boolean test(MedicalPreparation medicalPreparation) throws Exception {

                        boolean isMedicalPreparationHaveValues = (medicalPreparation != null);

                        if (!isMedicalPreparationHaveValues) {
                            Log.i(App.getInstance().getString(R.string.TAG_get_Preparations_By_Id),
                                    App.getInstance().getString(R.string.list_med_preparation_not_values));
                        }

                        return isMedicalPreparationHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<MedicalPreparation>() {
                    @Override
                    public void accept(MedicalPreparation medicalPreparation) throws Exception {
                        medicalPreparationLiveDataById.setValue(medicalPreparation);
                        iMedicalPreparation.getMedicalPreparation(medicalPreparation);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e(App.getInstance().getString(R.string.TAG_get_Preparations_By_Id),
                                throwable.getMessage(), throwable);
                    }
                }));
    }

    public void fetchMedicalPreparationList() {
        disposables.add(ApiService.getMedicalPreparations()

                .map(new Function<ResponseModel, List<MedicalPreparation>>() {
                    @Override
                    public List<MedicalPreparation> apply(ResponseModel responseModel) {
                        final List<MedicalPreparation> medicalPreparationList = convert(responseModel);
                        return medicalPreparationList;
                    }
                })

                .filter(new Predicate<List<MedicalPreparation>>() {
                    @Override
                    public boolean test(List<MedicalPreparation> medicalPreparationList) throws Exception {

                        boolean isMedicalPreparationListHaveValues = ((medicalPreparationList != null) && (!medicalPreparationList.isEmpty()));

                        if (!isMedicalPreparationListHaveValues) {
                            Log.i(App.getInstance().getString(R.string.TAG_get_Preparations_List),
                                    App.getInstance().getString(R.string.list_med_preparation_not_values));
                        }

                        return isMedicalPreparationListHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<MedicalPreparation>>() {
                    @Override
                    public void accept(List<MedicalPreparation> medicalPreparationList) throws Exception {
                        medicalPreparationsLiveData.setValue(medicalPreparationList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e(App.getInstance().getString(R.string.TAG_get_Preparations_List),
                                throwable.getMessage(), throwable);
                    }
                }));
    }

    public void fetchMedicalPreparationListByPageNumber(String pageNumber) {
        disposables.add(ApiService.getMedicalPreparationsByPageNumber(pageNumber)

                .map(new Function<ResponseModel, List<MedicalPreparation>>() {
                    @Override
                    public List<MedicalPreparation> apply(ResponseModel responseModel) {
                        final List<MedicalPreparation> medicalPreparationList = convert(responseModel);
                        return medicalPreparationList;
                    }
                })

                .filter(new Predicate<List<MedicalPreparation>>() {
                    @Override
                    public boolean test(List<MedicalPreparation> medicalPreparationList) throws Exception {

                        boolean isMedicalPreparationListHaveValues = ((medicalPreparationList != null) && (!medicalPreparationList.isEmpty()));

                        if (!isMedicalPreparationListHaveValues) {
                            Log.i(App.getInstance().getString(R.string.TAG_get_Preparations_List),
                                    App.getInstance().getString(R.string.list_med_preparation_not_values));
                        }

                        return isMedicalPreparationListHaveValues;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<List<MedicalPreparation>>() {
                    @Override
                    public void accept(List<MedicalPreparation> medicalPreparationList) throws Exception {



                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e(App.getInstance().getString(R.string.TAG_get_Preparations_List),
                                throwable.getMessage(), throwable);
                    }
                }));
    }

    public static MedicalPreparation convertResponseModelId(ResponseModelId responseModelId) {

        int id = responseModelId.getId();
        String tradeLabel = responseModelId.getTradeLabel().getName();
        String manufacturerName = responseModelId.getManufacturer().getName();
        String packagingDescription = responseModelId.getPackaging().getDescription();
        String compositionDescription = responseModelId.getComposition().getDescription();
        String compositionInn = responseModelId.getComposition().getInn().getName();
        String compositionPharmForm = responseModelId.getComposition().getPharmForm().getName();

        MedicalPreparation medicalPreparation = new MedicalPreparation(id, tradeLabel, manufacturerName,
                packagingDescription, compositionDescription, compositionInn, compositionPharmForm);

        return medicalPreparation;
    }

    public static List<MedicalPreparation> convert(ResponseModel responseModel) {

        List<MedicalPreparation> medicalPreparations = new ArrayList<>();
        List<ResponseModel.Result> results = responseModel.getResults();

        if (results != null) {
            for (int i = 0; i < results.size(); i++) {

                int id = 0;
                if (results.get(i).getId() != null) {
                    id = results.get(i).getId();
                }

                String tradeLabel = "";
                if (results.get(i).getTradeLabel() != null) {
                    tradeLabel = results.get(i).getTradeLabel().getName();
                }

                String manufacturerName = "";
                if (results.get(i).getManufacturer() != null) {
                    manufacturerName = results.get(i).getManufacturer().getName();
                }

                String packagingDescription = "";
                if (results.get(i).getPackaging() != null) {
                    packagingDescription = results.get(i).getPackaging().getDescription();
                }

                String compositionDescription = "";
                if (results.get(i).getComposition() != null) {
                    compositionDescription = results.get(i).getComposition().getDescription();
                }

                String compositionInn = "";
                if (results.get(i).getComposition() != null || results.get(i).getComposition().getInn() != null) {
                    compositionInn = results.get(i).getComposition().getInn().getName();
                }

                String compositionPharmForm = "";
                if (results.get(i).getComposition() != null || results.get(i).getComposition().getPharmForm() != null) {
                    compositionPharmForm = results.get(i).getComposition().getPharmForm().getName();
                }

                MedicalPreparation medicalPreparation = new MedicalPreparation(id, tradeLabel, manufacturerName,
                        packagingDescription, compositionDescription, compositionInn, compositionPharmForm);

                medicalPreparations.add(medicalPreparation);
            }
        }
        return medicalPreparations;
    }
}
