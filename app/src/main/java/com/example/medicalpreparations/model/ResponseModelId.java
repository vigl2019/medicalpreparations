package com.example.medicalpreparations.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModelId {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("composition")
    @Expose
    private Composition composition;
    @SerializedName("packaging")
    @Expose
    private Packaging packaging;
    @SerializedName("trade_label")
    @Expose
    private TradeLabel tradeLabel;
    @SerializedName("manufacturer")
    @Expose
    private Manufacturer manufacturer;
    @SerializedName("code")
    @Expose
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Composition getComposition() {
        return composition;
    }

    public void setComposition(Composition composition) {
        this.composition = composition;
    }

    public Packaging getPackaging() {
        return packaging;
    }

    public void setPackaging(Packaging packaging) {
        this.packaging = packaging;
    }

    public TradeLabel getTradeLabel() {
        return tradeLabel;
    }

    public void setTradeLabel(TradeLabel tradeLabel) {
        this.tradeLabel = tradeLabel;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public class Composition {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("atc")
        @Expose
        private List<String> atc = null;
        @SerializedName("inn")
        @Expose
        private Inn inn;
        @SerializedName("pharm_form")
        @Expose
        private PharmForm pharmForm;
        @SerializedName("dosage")
        @Expose
        private Double dosage;
        @SerializedName("measure")
        @Expose
        private Measure measure;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getAtc() {
            return atc;
        }

        public void setAtc(List<String> atc) {
            this.atc = atc;
        }

        public Inn getInn() {
            return inn;
        }

        public void setInn(Inn inn) {
            this.inn = inn;
        }

        public PharmForm getPharmForm() {
            return pharmForm;
        }

        public void setPharmForm(PharmForm pharmForm) {
            this.pharmForm = pharmForm;
        }

        public Double getDosage() {
            return dosage;
        }

        public void setDosage(Double dosage) {
            this.dosage = dosage;
        }

        public Measure getMeasure() {
            return measure;
        }

        public void setMeasure(Measure measure) {
            this.measure = measure;
        }
    }

    public class Country {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("iso2")
        @Expose
        private String iso2;
        @SerializedName("iso3")
        @Expose
        private String iso3;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIso2() {
            return iso2;
        }

        public void setIso2(String iso2) {
            this.iso2 = iso2;
        }

        public String getIso3() {
            return iso3;
        }

        public void setIso3(String iso3) {
            this.iso3 = iso3;
        }
    }

    public class Inn {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Manufacturer {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("country")
        @Expose
        private Country country;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Country getCountry() {
            return country;
        }

        public void setCountry(Country country) {
            this.country = country;
        }
    }

    public class Measure {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("iso")
        @Expose
        private String iso;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIso() {
            return iso;
        }

        public void setIso(String iso) {
            this.iso = iso;
        }
    }

    public class Packaging {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("composition")
        @Expose
        private Composition composition;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("in_bulk")
        @Expose
        private Boolean inBulk;
        @SerializedName("minimal_quantity")
        @Expose
        private String minimalQuantity;
        @SerializedName("package_quantity")
        @Expose
        private String packageQuantity;
        @SerializedName("variant")
        @Expose
        private Variant variant;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Composition getComposition() {
            return composition;
        }

        public void setComposition(Composition composition) {
            this.composition = composition;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Boolean getInBulk() {
            return inBulk;
        }

        public void setInBulk(Boolean inBulk) {
            this.inBulk = inBulk;
        }

        public String getMinimalQuantity() {
            return minimalQuantity;
        }

        public void setMinimalQuantity(String minimalQuantity) {
            this.minimalQuantity = minimalQuantity;
        }

        public String getPackageQuantity() {
            return packageQuantity;
        }

        public void setPackageQuantity(String packageQuantity) {
            this.packageQuantity = packageQuantity;
        }

        public Variant getVariant() {
            return variant;
        }

        public void setVariant(Variant variant) {
            this.variant = variant;
        }
    }

    public class PharmForm {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("short_name")
        @Expose
        private String shortName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShortName() {
            return shortName;
        }

        public void setShortName(String shortName) {
            this.shortName = shortName;
        }
    }

    public class TradeLabel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Variant {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("pharm_form")
        @Expose
        private PharmForm pharmForm;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("short_name")
        @Expose
        private String shortName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public PharmForm getPharmForm() {
            return pharmForm;
        }

        public void setPharmForm(PharmForm pharmForm) {
            this.pharmForm = pharmForm;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShortName() {
            return shortName;
        }

        public void setShortName(String shortName) {
            this.shortName = shortName;
        }
    }
}
