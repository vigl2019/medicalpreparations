package com.example.medicalpreparations.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.databinding.BaseObservable;

public class MedicalPreparation extends BaseObservable implements Parcelable {

    private int id;
    private String tradeLabel;
    private String manufacturerName;
    private String packagingDescription;
    private String compositionDescription;
    private String compositionInn;
    private String compositionPharmForm;

    public MedicalPreparation(){}

    public MedicalPreparation(int id, String tradeLabel, String manufacturerName, String packagingDescription, String compositionDescription, String compositionInn, String compositionPharmForm) {
        this.id = id;
        this.tradeLabel = tradeLabel;
        this.manufacturerName = manufacturerName;
        this.packagingDescription = packagingDescription;
        this.compositionDescription = compositionDescription;
        this.compositionInn = compositionInn;
        this.compositionPharmForm = compositionPharmForm;
    }

    protected MedicalPreparation(Parcel in) {
        id = in.readInt();
        tradeLabel = in.readString();
        manufacturerName = in.readString();
        packagingDescription = in.readString();
        compositionDescription = in.readString();
        compositionInn = in.readString();
        compositionPharmForm = in.readString();
    }

    public static final Creator<MedicalPreparation> CREATOR = new Creator<MedicalPreparation>() {
        @Override
        public MedicalPreparation createFromParcel(Parcel in) {
            return new MedicalPreparation(in);
        }

        @Override
        public MedicalPreparation[] newArray(int size) {
            return new MedicalPreparation[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTradeLabel() {
        return tradeLabel;
    }

    public void setTradeLabel(String tradeLabel) {
        this.tradeLabel = tradeLabel;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getPackagingDescription() {
        return packagingDescription;
    }

    public void setPackagingDescription(String packagingDescription) {
        this.packagingDescription = packagingDescription;
    }

    public String getCompositionDescription() {
        return compositionDescription;
    }

    public void setCompositionDescription(String compositionDescription) {
        this.compositionDescription = compositionDescription;
    }

    public String getCompositionInn() {
        return compositionInn;
    }

    public void setCompositionInn(String compositionInn) {
        this.compositionInn = compositionInn;
    }

    public String getCompositionPharmForm() {
        return compositionPharmForm;
    }

    public void setCompositionPharmForm(String compositionPharmForm) {
        this.compositionPharmForm = compositionPharmForm;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(tradeLabel);
        parcel.writeString(manufacturerName);
        parcel.writeString(packagingDescription);
        parcel.writeString(compositionDescription);
        parcel.writeString(compositionInn);
        parcel.writeString(compositionPharmForm);
    }
}