package com.example.medicalpreparations.api;

import com.example.medicalpreparations.R;
import com.example.medicalpreparations.model.ResponseModel;
import com.example.medicalpreparations.model.ResponseModelId;
import com.example.medicalpreparations.utils.App;

import io.reactivex.Flowable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiService {

    private static final String BASE_URL = App.getInstance().getString(R.string.base_url);
    private static PrivateApi privateApi;

    public interface PrivateApi {

        @GET("v1/medicine/")
        Flowable<ResponseModel> getMedicalPreparationsByPageNumber(@Query("pageNumber") String pageNumber);

        @GET("v1/medicine/")
        Flowable<ResponseModel> getMedicalPreparations();

        /*
                @GET("v1/medicine/")
                Flowable<ResponseModel> getMedicalPreparations(@Query("format") String format);
        */
        @GET("v1/medicine/")
        Flowable<ResponseModel> getMedicalPreparationByTitle(@Query("search") String search);

        @GET("v1/medicine/{id}")
        Flowable<ResponseModelId> getMedicalPreparationById(@Path("id") String id);
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Flowable<ResponseModel> getMedicalPreparationsByPageNumber(String pageNumber) {
        return privateApi.getMedicalPreparationsByPageNumber(pageNumber);
    }

    public static Flowable<ResponseModel> getMedicalPreparations() {
        return privateApi.getMedicalPreparations();
    }

    /*
        public static Flowable<ResponseModel> getMedicalPreparations(String format) {
            return privateApi.getMedicalPreparations(format);
        }
    */
    public static Flowable<ResponseModel> getMedicalPreparationByTitle(String search) {
        return privateApi.getMedicalPreparationByTitle(search);
    }

    public static Flowable<ResponseModelId> getMedicalPreparationById(String id) {
        return privateApi.getMedicalPreparationById(id);
    }
}
