package com.example.medicalpreparations.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.medicalpreparations.R;
import com.example.medicalpreparations.databinding.ActivityMainBinding;
import com.example.medicalpreparations.model.MedicalPreparation;
import com.example.medicalpreparations.utils.Helper;
import com.example.medicalpreparations.viewmodel.MedicalPreparationsViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MedicalPreparationsViewModel medicalPreparationsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupBindings(savedInstanceState);
    }

    private void setupBindings(Bundle savedInstanceState) {
        ActivityMainBinding activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        medicalPreparationsViewModel = ViewModelProviders.of(this).get(MedicalPreparationsViewModel.class);
        if (savedInstanceState == null) {
            medicalPreparationsViewModel.init();
        }
        activityBinding.setModel(medicalPreparationsViewModel);
        medicalPreparationListUpdate();
    }

    private void medicalPreparationListUpdate() {
        medicalPreparationsViewModel.fetchMedicalPreparationList();
        medicalPreparationsViewModel.getMedicalPreparationsLiveData().observe(this, new Observer<List<MedicalPreparation>>() {
            @Override
            public void onChanged(List<MedicalPreparation> medicalPreparations) {
                if (medicalPreparations.size() == 0) {
                    medicalPreparationsViewModel.showEmpty.set(View.VISIBLE);
                } else {
                    medicalPreparationsViewModel.showEmpty.set(View.GONE);
                    medicalPreparationsViewModel.setMedicalPreparationsInAdapter(medicalPreparations);
                }
            }
        });
        medicalPreparationListClick();
    }

    private void medicalPreparationListClick() {
        medicalPreparationsViewModel.getSelected().observe(this, new Observer<MedicalPreparation>() {
            @Override
            public void onChanged(MedicalPreparation medicalPreparation) {
                if (medicalPreparation != null) {

                    Intent intent = new Intent(MainActivity.this, MedicalPreparationDetailsActivity.class);
                    intent.putExtra(getApplicationContext().getString(R.string.medical_preparation_id),
                                    Integer.toString(medicalPreparation.getId()));
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search_by_title).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryRefinementEnabled(true);
        searchView.setQueryHint(getApplicationContext().getString(R.string.search_query_hint));

        final List<String> searchSuggestions = new ArrayList<>();

        medicalPreparationsViewModel.getMedicalPreparationsLiveData().observe(this, new Observer<List<MedicalPreparation>>() {
            @Override
            public void onChanged(List<MedicalPreparation> medicalPreparations) {

                for (MedicalPreparation medicalPreparation : medicalPreparations) {
                    searchSuggestions.add(medicalPreparation.getCompositionInn() + " \n"
                            + medicalPreparation.getTradeLabel());
                }
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_dropdown_item_1line_extended, searchSuggestions);
        final SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchAutoComplete.setAdapter(adapter);

        searchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String searchString = (String)parent.getItemAtPosition(position);
                searchAutoComplete.setText(searchString.trim());

                Intent intent = new Intent(MainActivity.this, MedicalPreparationSearchActivity.class);
                intent.putExtra(getApplicationContext().getString(R.string.medical_preparation_title),
                                Helper.getMedicalPreparationTitle(searchString.trim()));
                startActivity(intent);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Intent intent = new Intent(MainActivity.this, MedicalPreparationSearchActivity.class);
                intent.putExtra(getApplicationContext().getString(R.string.medical_preparation_title),
                                Helper.getMedicalPreparationTitle(query));
                startActivity(intent);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return false;
            }
        });

        return true;
    }
}
