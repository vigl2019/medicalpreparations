package com.example.medicalpreparations.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medicalpreparations.R;
import com.example.medicalpreparations.adapter.SearchResultsAdapter;
import com.example.medicalpreparations.model.MedicalPreparation;
import com.example.medicalpreparations.model.MedicalPreparations;
import com.example.medicalpreparations.utils.IMedicalPreparations;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicalPreparationSearchActivity extends AppCompatActivity {

    @BindView(R.id.list_medical_preparations)
    RecyclerView medicalPreparationsRecyclerView;

    private String medicalPreparationTitle;
    private SearchResultsAdapter searchResultsAdapter;
    private MedicalPreparations medicalPreparations = new MedicalPreparations();

    public IMedicalPreparations iMedicalPreparations = new IMedicalPreparations() {
        @Override
        public void getMedicalPreparations(List<MedicalPreparation> medicalPreparations) {
            recyclerViewInit(medicalPreparations);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_medical_preparation);
        ButterKnife.bind(this);
        medicalPreparationTitle = getIntent().getStringExtra(getApplicationContext().getString(R.string.medical_preparation_title));
        medicalPreparations.fetchMedicalPreparationListByTitle(medicalPreparationTitle, iMedicalPreparations);
    }

    private void recyclerViewInit(List<MedicalPreparation> medicalPreparations) {
        searchResultsAdapter = new SearchResultsAdapter(medicalPreparations);
        medicalPreparationsRecyclerView.setAdapter(searchResultsAdapter);
        searchResultsAdapter.setListener(new SearchResultsAdapter.OnItemClickListener() {
            @Override
            public void onClick(MedicalPreparation medicalPreparation) {
                Intent intent = new Intent(MedicalPreparationSearchActivity.this, MedicalPreparationDetailsActivity.class);
                intent.putExtra(getApplicationContext().getString(R.string.medical_preparation_id),
                                Integer.toString(medicalPreparation.getId()));
                startActivity(intent);
            }
        });
    }
}