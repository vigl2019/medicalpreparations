package com.example.medicalpreparations.view;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.medicalpreparations.R;
import com.example.medicalpreparations.model.MedicalPreparation;
import com.example.medicalpreparations.model.MedicalPreparations;
import com.example.medicalpreparations.utils.IMedicalPreparation;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicalPreparationDetailsActivity extends AppCompatActivity {

    @BindView(R.id.ampd_trade_label)
    TextView tradeLabel;

    @BindView(R.id.ampd_manufacturer_name)
    TextView manufacturerName;

    @BindView(R.id.ampd_packaging_description)
    TextView packagingDescription;

    @BindView(R.id.ampd_composition_description)
    TextView compositionDescription;

    @BindView(R.id.ampd_composition_inn)
    TextView compositionInn;

    @BindView(R.id.ampd_composition_pharm_form)
    TextView compositionPharmForm;

    private String medicalPreparationId;
    private MedicalPreparations medicalPreparations = new MedicalPreparations();

    public IMedicalPreparation iMedicalPreparation = new IMedicalPreparation() {
        @Override
        public void getMedicalPreparation(MedicalPreparation medicalPreparation) {
            setTextView(medicalPreparation);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_preparation_details);
        ButterKnife.bind(this);
        medicalPreparationId = getIntent().getStringExtra(getApplicationContext().getString(R.string.medical_preparation_id));
        medicalPreparations.fetchMedicalPreparationById(medicalPreparationId, iMedicalPreparation);
    }

    private void setTextView(MedicalPreparation medicalPreparationDetails){
        tradeLabel.setText(medicalPreparationDetails.getTradeLabel());
        manufacturerName.setText(medicalPreparationDetails.getManufacturerName());
        packagingDescription.setText(medicalPreparationDetails.getPackagingDescription());
        compositionDescription.setText(medicalPreparationDetails.getCompositionDescription());
        compositionInn.setText(medicalPreparationDetails.getCompositionInn());
        compositionPharmForm.setText(medicalPreparationDetails.getCompositionPharmForm());
    }
}

